import typing as tp

from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import IntEnum


class AudioPlayerStreamType(IntEnum):
    NONE = 0
    ARTIST = 1
    PLAYLIST = 2


@dataclass
class CurTrackInfo:
    name: str
    artist: str


@dataclass
class AudioPlayerState:
    stream_type: AudioPlayerStreamType = AudioPlayerStreamType.NONE
    is_playing: bool = False
    cur_track_info: tp.Optional[CurTrackInfo] = None


class IPlayer(ABC):
    @property
    @abstractmethod
    def state(self) -> AudioPlayerState:
        raise NotImplementedError

    @abstractmethod
    def play_playlist(self, playlist_id: int, user_name: tp.Optional[str] = None) -> None:
        raise NotImplementedError

    @abstractmethod
    def play_artist(self, artist_id: int) -> None:
        raise NotImplementedError

    @abstractmethod
    def play_album(self, album_id: int) -> None:
        raise NotImplementedError

    @abstractmethod
    def play_track(self, album_id: int, track_id: int) -> None:
        raise NotImplementedError

    @abstractmethod
    def toggle_play_pause(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def pause(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def resume(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def next_track(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def prev_track(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def close(self) -> None:
        raise NotImplementedError
