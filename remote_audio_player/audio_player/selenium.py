import typing as tp

from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver


def get_available_driver() -> tp.Optional[WebDriver]:
    for driver_cls in [webdriver.Firefox, webdriver.Chrome]:
        try:
            return driver_cls()
        except Exception:
            pass
    return None
