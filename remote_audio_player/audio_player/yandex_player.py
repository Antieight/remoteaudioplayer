import time

import typing as tp

from dataclasses import dataclass

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from .common import IPlayer, AudioPlayerStreamType, AudioPlayerState, CurTrackInfo
from .selenium import get_available_driver



@dataclass
class YandexMusicAudioPlayer(IPlayer):
    session_id: str
    user_name: str = 'igor-darov'

    _driver: tp.Optional[webdriver.firefox.webdriver.WebDriver] = None
    _yandex_music_base_address: tp.ClassVar[str] = 'https://music.yandex.ru'

    def _set_cookies_for_yandex_music(self) -> None:
        self._driver.get(self._yandex_music_base_address)
        for c_name, c_value in {'Session_id': self.session_id, 'sessionid2': self.session_id}.items():
            self._driver.add_cookie({'name': c_name, 'value': c_value})

    def __post_init__(self) -> None:
        assert self.session_id, 'set SESSION_ID env var'
        self._driver = get_available_driver()
        assert self._driver is not None, 'Unable to find driver for selenium. Install geckodriver or chromedriver'
        self._set_cookies_for_yandex_music()

    def _click_play_button(self) -> None:
        try:
            self._driver.find_element(By.CLASS_NAME, 'button-play').click()
        except Exception:
            pass

    def _click_play_button_on_the_side_bar(self) -> None:
        try:
            self._driver \
                .find_element(By.CLASS_NAME, 'sidebar__controls') \
                .find_element(By.CLASS_NAME, 'button-play') \
                .click()
        except Exception:
            pass

    @property
    def _cur_stream_type(self) -> AudioPlayerStreamType:
        cur_url = self._driver.current_url
        assert cur_url.startswith(self._yandex_music_base_address), cur_url
        https, _, cut_base_address, *_, handle, _ = cur_url.split('/')
        return {
            'playlists': AudioPlayerStreamType.PLAYLIST,
            'artist': AudioPlayerStreamType.ARTIST,
            'home': AudioPlayerStreamType.NONE,
        }[handle]

    @property
    def _is_playing(self) -> bool:
        try:
            self._driver.find_element(By.CLASS_NAME, 'player-controls__btn_pause')
            return True
        except NoSuchElementException:
            return False

    def _safely_get_title_by_path(self, *path_to_element: str, default: str = 'Unknown') -> str:
        try:
            el = self._driver
            for path_part in path_to_element:
                el = el.find_element(By.CLASS_NAME, path_part)
            return el.get_attribute('title')
        except Exception as e:
            return default

    @property
    def _cur_track_info(self) -> CurTrackInfo:
        return CurTrackInfo(
            name=self._safely_get_title_by_path('track__title'),
            artist=self._safely_get_title_by_path('d-artists__expanded', 'd-link')
        )

    @property
    def state(self) -> AudioPlayerState:
        return AudioPlayerState(
            stream_type=self._cur_stream_type,
            is_playing=self._is_playing,
            cur_track_info=self._cur_track_info
        )

    def click_play_button_until_playing(self, click_fn: tp.Callable,
                                        sleep_time: float = 1., n_retries: int = 7) -> None:
        for _ in range(n_retries):
            time.sleep(sleep_time)
            click_fn()
            if self._is_playing:
                return

    def play_playlist(self, playlist_id: int, user_name: tp.Optional[str] = None) -> None:
        user_name = user_name or self.user_name
        url = f'{self._yandex_music_base_address}/users/{user_name}/playlists/{playlist_id}'
        self._driver.get(url)
        self.click_play_button_until_playing(self._click_play_button)

    def play_artist(self, artist_id: int) -> None:
        url = f'{self._yandex_music_base_address}/artist/{artist_id}'
        self._driver.get(url)
        self.click_play_button_until_playing(self._click_play_button)

    def play_album(self, album_id: int) -> None:
        url = f'{self._yandex_music_base_address}/album/{album_id}'
        self._driver.get(url)
        self.click_play_button_until_playing(self._click_play_button)

    def play_track(self, album_id: int, track_id: int) -> None:
        url = f'{self._yandex_music_base_address}/album/{album_id}/track/{track_id}'
        self._driver.get(url)
        self.click_play_button_until_playing(self._click_play_button_on_the_side_bar)

    def toggle_play_pause(self) -> None:
        self._driver.switch_to.active_element.send_keys(' ')

    def next_track(self) -> None:
        self._driver.switch_to.active_element.send_keys('L')

    def prev_track(self) -> None:
        self._driver.switch_to.active_element.send_keys('K')

    def pause(self) -> None:
        if self._is_playing:
            self.toggle_play_pause()

    def resume(self) -> None:
        if not self._is_playing:
            self.toggle_play_pause()

    def close(self) -> None:
        self._driver.close()
