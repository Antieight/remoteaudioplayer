import os

import click

from .flask_server import start_flask_server
from .telegram_bot import TelegramBot
from .scheduler import start_scheduler


@click.command()
@click.option('-p', '--port', required=True, type=int)
@click.option('-t', '--tg-bot-token', default=lambda: os.environ.get('REMOTE_AUDIO_PLAYER_TELEGRAM_BOT_TOKEN', ''),
              help='Token for telegram bot. By default is read from env variable REMOTE_AUDIO_PLAYER_TELEGRAM_BOT_TOKEN')
def start_server(port: int, tg_bot_token: str) -> None:
    telegram_bot = TelegramBot(tg_bot_token=tg_bot_token)
    telegram_bot.start_polling()
    print('Started telegram bot')

    start_flask_server(port=port)
    print(f'Started flask server at {port=}')

    start_scheduler(check_every_n_seconds=1)
    print(f'Started scheduler')
