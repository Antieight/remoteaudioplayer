from threading import Lock
from .objects import ClientInfo


# state is secured by global_lock
global_lock: Lock = Lock()
client_id_to_client_info: dict[int, ClientInfo] = {}
