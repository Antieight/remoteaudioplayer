from __future__ import annotations

import datetime
import time
from dataclasses import dataclass
from threading import Thread
from ..state import global_lock, client_id_to_client_info


@dataclass
class Scheduler:
    check_every_n_seconds: int

    @staticmethod
    def _check_schedule() -> None:
        with global_lock:
            for client_info in client_id_to_client_info.values():
                this_client_current_datetime = (
                    datetime.datetime.utcnow() +
                    datetime.timedelta(hours=client_info.tz_timedelta_hours)
                )
                updated_schedule = []
                for schedule_entry in client_info.schedule:
                    if schedule_entry.next_occurrence_datetime > this_client_current_datetime:
                        updated_schedule.append(schedule_entry)
                    else:
                        client_info.directives_queue.append(schedule_entry.directive)
                        updated_schedule.extend(schedule_entry.get_next_scheduled_events())

                client_info.schedule = updated_schedule

    def start(self) -> None:
        while True:
            time.sleep(self.check_every_n_seconds)
            self._check_schedule()


def start_scheduler(check_every_n_seconds: int = 1) -> None:
    Thread(target=Scheduler(check_every_n_seconds=check_every_n_seconds).start).run()
