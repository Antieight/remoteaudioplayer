from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from .callbacks_data import CallbackData


############
### Util ###
############

_PLAYER_BUTTONS_ROW = [
    InlineKeyboardButton(text='⏮︎', callback_data=CallbackData.PREV.value),
    InlineKeyboardButton(text='⏹', callback_data=CallbackData.PAUSE.value),
    InlineKeyboardButton(text='▶', callback_data=CallbackData.RESUME.value),
    InlineKeyboardButton(text='⏭', callback_data=CallbackData.NEXT.value),
]

_CANCEL_BUTTON_ROW = [
    InlineKeyboardButton(text='cancel', callback_data=CallbackData.CANCEL.value)
]

_CHOOSE_TYPE_OF_PLAYBACK_DIRECTIVE_BUTTONS_ROWS = [
    [
        InlineKeyboardButton(text='playlist', callback_data=CallbackData.PLAYLIST.value),
        InlineKeyboardButton(text='artist', callback_data=CallbackData.ARTIST.value),
    ],
    [
        InlineKeyboardButton(text='album', callback_data=CallbackData.ALBUM.value),
        InlineKeyboardButton(text='track', callback_data=CallbackData.TRACK.value),
    ],
]

#######################
### Buttons markups ###
#######################

MAIN_MENU_BUTTONS_MARKUP = InlineKeyboardMarkup([
    [
        InlineKeyboardButton(text='play', callback_data=CallbackData.PLAY.value),
        InlineKeyboardButton(text='schedule', callback_data=CallbackData.SCHEDULE.value),
    ],
    [
        InlineKeyboardButton(text='status', callback_data=CallbackData.STATUS.value),
    ],
    _PLAYER_BUTTONS_ROW,
])


SCHEDULE_ID_BUTTONS_MARKUP = InlineKeyboardMarkup([
    [
        InlineKeyboardButton(text='generate random', callback_data=CallbackData.SCHEDULE_GENERATE_RANDOM_ID.value),
    ],
    _CANCEL_BUTTON_ROW,
])

SCHEDULE_DIRECTIVE_BUTTONS_MARKUP = InlineKeyboardMarkup([
    [
        InlineKeyboardButton(text='pause', callback_data=CallbackData.SCHEDULE_PAUSE.value),
    ],
    *_CHOOSE_TYPE_OF_PLAYBACK_DIRECTIVE_BUTTONS_ROWS,
    _CANCEL_BUTTON_ROW,
])

SCHEDULE_PLAY_DURATION_BUTTONS_MARKUP = InlineKeyboardMarkup([
    [
        InlineKeyboardButton(text='unlimited play duration',
                             callback_data=CallbackData.SCHEDULE_UNLIMITED_PLAY_DURATION.value),
    ],
    _CANCEL_BUTTON_ROW,
])

SCHEDULE_REPEAT_ON_WEEKDAYS_BUTTONS_MARKUP = InlineKeyboardMarkup([
    [
        InlineKeyboardButton(text='do not repeat', callback_data=CallbackData.SCHEDULE_DO_NOT_REPEAT.value),
    ],
    _CANCEL_BUTTON_ROW,
])

SCHEDULE_UNTIL_DATE_BUTTONS_MARKUP = InlineKeyboardMarkup([
    [
        InlineKeyboardButton(text='no until-date', callback_data=CallbackData.SCHEDULE_NO_UNTIL_DATE.value),
    ],
    _CANCEL_BUTTON_ROW,
])

SCHEDULE_CONFIRM_BUTTONS_MARKUP = InlineKeyboardMarkup([
    [
        InlineKeyboardButton(text='yes', callback_data=CallbackData.SCHEDULE_CONFIRM.value),
        InlineKeyboardButton(text='no', callback_data=CallbackData.CANCEL.value),
    ],
])


PLAY_CHOOSE_PLAYBACK_TYPE_BUTTONS_MARKUP = InlineKeyboardMarkup([
    *_CHOOSE_TYPE_OF_PLAYBACK_DIRECTIVE_BUTTONS_ROWS,
    _CANCEL_BUTTON_ROW,
])


STATUS_BUTTONS_MARKUP = InlineKeyboardMarkup([
    [
        InlineKeyboardButton(text='delete scheduled event',
                             callback_data=CallbackData.STATUS_DELETE_SCHEDULED_EVENT.value),
        InlineKeyboardButton(text='clear scheduled events',
                             callback_data=CallbackData.STATUS_CLEAR_SCHEDULED_EVENTS.value),
    ],
    _CANCEL_BUTTON_ROW,
])


CANCEL_BUTTON_MARKUP = InlineKeyboardMarkup([
    _CANCEL_BUTTON_ROW,
])
