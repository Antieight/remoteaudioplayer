import datetime
import hashlib
import random
import typing as tp
from dataclasses import dataclass

from telegram import Update, Message, ParseMode
from telegram.ext import (
    Updater, Filters, CallbackContext, ConversationHandler,
    CallbackQueryHandler, CommandHandler, MessageHandler,
)

from .callbacks_data import CallbackData
from .forms import Form, PlayForm, LoginForm, ScheduleForm, DeleteScheduledEventForm, DirectiveType
from .markups import (
    MAIN_MENU_BUTTONS_MARKUP, STATUS_BUTTONS_MARKUP, PLAY_CHOOSE_PLAYBACK_TYPE_BUTTONS_MARKUP,
    SCHEDULE_PLAY_DURATION_BUTTONS_MARKUP, SCHEDULE_UNTIL_DATE_BUTTONS_MARKUP, CANCEL_BUTTON_MARKUP,
    SCHEDULE_REPEAT_ON_WEEKDAYS_BUTTONS_MARKUP, SCHEDULE_ID_BUTTONS_MARKUP, SCHEDULE_DIRECTIVE_BUTTONS_MARKUP,
    SCHEDULE_CONFIRM_BUTTONS_MARKUP,
)
from .states import *
from ..objects import ScheduledEvent, ScheduledSingleEvent, ScheduledRepeatingEvent
from ..state import global_lock, client_id_to_client_info
from ...directives import (
    AudioPlayerPauseDirective, AudioPlayerResumeDirective, AudioPlayerNextTrackDirective,
    AudioPlayerPrevTrackDirective, AudioPlayerPlayArtistDirective, AudioPlayerPlayPlaylistDirective,
    AudioPlayerPlayAlbumDirective, AudioPlayerPlayTrackDirective, AudioPlayerDirective
)


@dataclass
class ClientAuthenticationInfo:
    client_id: int
    passphrase_hash: str


@dataclass
class ChatState:
    client_authentication_info: tp.Optional[ClientAuthenticationInfo] = None
    active_form: tp.Optional[Form] = None


class TelegramBot:
    start_message_text = (
        'Hi! I am RemoteAudioPlayer bot. I will help you with setting up your remote player.\n\n'
        'You should first tell me the client_id of the player you want to control\n'
        'To reset it later use /start'
    )

    def __init__(self, tg_bot_token: str) -> None:
        self._updater = Updater(token=tg_bot_token, use_context=True)

        message_filters = Filters.text & ~Filters.command
        conv_handler = ConversationHandler(
            entry_points=[CommandHandler('start', self.start_handler)],
            states={
                LOGIN_STATE: [
                    MessageHandler(filters=message_filters, callback=self.login_handler)
                ],
                PASSWORD_STATE: [
                    MessageHandler(filters=message_filters, callback=self.passphrase_handler)
                ],

                MAIN_MENU_STATE: [
                    CallbackQueryHandler(
                        callback=self.main_menu_button_schedule_handler,
                        pattern=CallbackData.SCHEDULE.pattern,
                    ),
                    CallbackQueryHandler(
                        callback=self.main_menu_button_play_handler,
                        pattern=CallbackData.PLAY.pattern,
                    ),
                    CallbackQueryHandler(
                        callback=self.main_menu_button_status_handler,
                        pattern=CallbackData.STATUS.pattern,
                    ),
                    CallbackQueryHandler(
                        callback=self.button_resume_handler,
                        pattern=CallbackData.RESUME.pattern,
                    ),
                    CallbackQueryHandler(
                        callback=self.button_pause_handler,
                        pattern=CallbackData.PAUSE.pattern,
                    ),
                    CallbackQueryHandler(
                        callback=self.button_previous_track_handler,
                        pattern=CallbackData.PREV.pattern,
                    ),
                    CallbackQueryHandler(
                        callback=self.button_next_track_handler,
                        pattern=CallbackData.NEXT.pattern,
                    ),
                ],

                SCHEDULE_ID_STATE: [
                    CallbackQueryHandler(
                        callback=self.generate_random_id_for_scheduled_event_handler,
                        pattern=CallbackData.SCHEDULE_GENERATE_RANDOM_ID.pattern,
                    ),
                    MessageHandler(
                        filters=message_filters,
                        callback=self.parse_scheduled_event_id_handler,
                    ),
                ],
                SCHEDULE_DATE_STATE: [
                    MessageHandler(
                        filters=message_filters,
                        callback=self.parse_scheduled_event_date_handler,
                    ),
                ],
                SCHEDULE_TIME_STATE: [
                    MessageHandler(
                        filters=message_filters,
                        callback=self.parse_scheduled_event_time_handler,
                    ),
                ],
                SCHEDULE_DIRECTIVE_STATE: [
                    CallbackQueryHandler(
                        callback=self.parse_scheduled_event_pause_directive_handler,
                        pattern=CallbackData.SCHEDULE_PAUSE.pattern,
                    ),
                    CallbackQueryHandler(
                        callback=self.parse_scheduled_event_directive_type_handler,
                        pattern='^directive_type',
                    ),
                ],
                SCHEDULE_DIRECTIVE_PLAY_STATE: [
                    MessageHandler(
                        callback=self.parse_scheduled_event_play_directive_handler,
                        filters=message_filters,
                    ),
                ],
                SCHEDULE_REPEAT_ON_WEEKDAYS_STATE: [
                    CallbackQueryHandler(
                        callback=self.scheduled_event_no_repeat_handler,
                        pattern=CallbackData.SCHEDULE_DO_NOT_REPEAT.pattern,
                    ),
                    MessageHandler(
                        callback=self.parse_scheduled_event_weekdays_handler,
                        filters=message_filters,
                    ),
                ],
                SCHEDULE_PLAY_DURATION_STATE: [
                    CallbackQueryHandler(
                        callback=self.schedule_unlimited_play_duration_handler,
                        pattern=CallbackData.SCHEDULE_UNLIMITED_PLAY_DURATION.pattern,
                    ),
                    MessageHandler(
                        callback=self.parse_scheduled_event_play_duration_handler,
                        filters=message_filters,
                    ),
                ],
                SCHEDULE_UNTIL_DATE_STATE: [
                    CallbackQueryHandler(
                        callback=self.schedule_no_until_date_handler,
                        pattern=CallbackData.SCHEDULE_NO_UNTIL_DATE.pattern,
                    ),
                    MessageHandler(
                        callback=self.parse_until_date_handler,
                        filters=message_filters,
                    )
                ],
                SCHEDULE_CONFIRM_STATE: [
                    CallbackQueryHandler(
                        callback=self.confirm_scheduled_event_handler,
                        pattern=CallbackData.SCHEDULE_CONFIRM.pattern,
                    ),
                ],

                PLAY_DIRECTIVE_STATE: [
                    CallbackQueryHandler(
                        callback=self.parse_play_event_directive_type_handler,
                        pattern='^directive_type',
                    ),
                ],
                PLAY_DIRECTIVE_PLAY_STATE: [
                    MessageHandler(
                        callback=self.parse_play_event_play_directive_handler,
                        filters=message_filters,
                    ),
                ],

                CURRENT_STATUS_STATE: [
                    CallbackQueryHandler(
                        callback=self.delete_schedule_entry_handler,
                        pattern=CallbackData.STATUS_DELETE_SCHEDULED_EVENT.pattern,
                    ),
                    CallbackQueryHandler(
                        callback=self.clear_scheduled_events_handler,
                        pattern=CallbackData.STATUS_CLEAR_SCHEDULED_EVENTS.pattern,
                    )
                ],
                CURRENT_STATUS_DELETE_ENTRY_STATE: [
                    MessageHandler(
                        callback=self.parse_scheduled_event_id_to_delete_handler,
                        filters=message_filters,
                    )
                ],
            },
            fallbacks=[
                CommandHandler('start', self.start_handler),
                CommandHandler('help', self.help_handler),
                CallbackQueryHandler(
                    callback=self.cancel_handler,
                    pattern=CallbackData.CANCEL.pattern,
                )
            ],
        )

        self._updater.dispatcher.add_handler(conv_handler)

    ################
    ##### UTIL #####
    ################

    @staticmethod
    def _chat_state(context: CallbackContext) -> ChatState:
        return tp.cast(ChatState, context.chat_data.setdefault('state', ChatState(active_form=LoginForm())))

    @staticmethod
    def _get_current_default_state(chat_state: ChatState) -> State:
        return MAIN_MENU_STATE if chat_state.client_authentication_info else LOGIN_STATE

    @staticmethod
    def _validate_authentication_info(chat_state: ChatState) -> None:
        client_id = chat_state.client_authentication_info.client_id
        passphrase_hash = chat_state.client_authentication_info.passphrase_hash
        with global_lock:
            if (
                client_id not in client_id_to_client_info or
                passphrase_hash != client_id_to_client_info[client_id].passphrase_hash
            ):
                chat_state.client_authentication_info = None
                chat_state.active_form = LoginForm()

    @staticmethod
    def _push_directive(context: CallbackContext, directive: AudioPlayerDirective) -> None:
        with global_lock:
            client_id = TelegramBot._chat_state(context).client_authentication_info.client_id
            client_id_to_client_info[client_id].directives_queue.append(directive)

    @staticmethod
    def _schedule_event(chat_state: ChatState, scheduled_event: ScheduledEvent) -> None:
        with global_lock:
            client_id = chat_state.client_authentication_info.client_id
            client_id_to_client_info[client_id].schedule.append(scheduled_event)

    @staticmethod
    def _parse_some_text_data(message: Message, callback: tp.Callable[[str], None], param_name: str) -> bool:
        try:
            callback(message.text)
            return True
        except Exception:  # noqa
            message.reply_text(f'Could not parse the {param_name} ("{message.text}")')
            return False

    @staticmethod
    def _cut_yandex_music_url(url: str) -> str:
        """https://music.yandex.com/SOME_VALID_PART?blablabla -> SOME_VALID_PART"""
        return url.removeprefix('https://music.yandex.com/') \
            .removeprefix('https://music.yandex.ru/') \
            .split('?')[0]

    @staticmethod
    def _parse_album(message_text: str) -> tp.Optional[AudioPlayerPlayAlbumDirective]:
        try:
            message_parts = message_text.replace('/', ' ').split()
            if len(message_parts) == 1:
                album_id_str = message_parts[0]
            elif len(message_parts) == 2:
                album, album_id_str = message_parts
                assert album == 'album'
            else:
                return None
            return AudioPlayerPlayAlbumDirective(album_id=int(album_id_str))
        except (AssertionError, ValueError):
            return None

    @staticmethod
    def _parse_artist(message_text: str) -> tp.Optional[AudioPlayerPlayArtistDirective]:
        try:
            message_parts = message_text.replace('/', ' ').split()
            if len(message_parts) == 1:
                artist_id_str = message_parts[0]
            elif len(message_parts) == 2:
                artist, artist_id_str = message_parts
                assert artist == 'artist'
            else:
                return None
            return AudioPlayerPlayArtistDirective(artist_id=int(artist_id_str))
        except (ValueError, AssertionError):
            return None

    @staticmethod
    def _parse_track(message_text: str) -> tp.Optional[AudioPlayerPlayTrackDirective]:
        try:
            message_parts = message_text.replace('/', ' ').split(' ')
            if len(message_parts) == 4:
                album, album_id_str, track, track_id_str = message_parts
                assert album == 'album' and track == 'track'
            elif len(message_parts) == 2:
                album_id_str, track_id_str = message_parts
            else:
                return None

            return AudioPlayerPlayTrackDirective(
                album_id=int(album_id_str),
                track_id=int(track_id_str)
            )
        except (AssertionError, ValueError):
            return None

    @staticmethod
    def _parse_playlist(message_text: str) -> tp.Optional[AudioPlayerPlayPlaylistDirective]:
        try:
            message_parts = message_text.replace('/', ' ').split(' ')
            if len(message_parts) == 4:
                users, user_name, playlists, playlist_id_str = message_parts
                assert users == 'users' and playlists == 'playlists'
            elif len(message_parts) == 2:
                user_name, playlist_id_str = message_parts
            else:
                return None
            return AudioPlayerPlayPlaylistDirective(playlist_id=int(playlist_id_str), user_name=user_name)
        except (AssertionError, ValueError):
            return None

    @staticmethod
    def _parse_any(message_text: str, directive_type: DirectiveType) -> tp.Optional[AudioPlayerDirective]:
        parser = {
            DirectiveType.TRACK: TelegramBot._parse_track,
            DirectiveType.ALBUM: TelegramBot._parse_album,
            DirectiveType.ARTIST: TelegramBot._parse_artist,
            DirectiveType.PLAYLIST: TelegramBot._parse_playlist,
        }[directive_type]
        return parser(message_text)

    @staticmethod
    def _make_scheduled_event(schedule_form: ScheduleForm) -> ScheduledEvent:
        if schedule_form.directive_type is None:
            return ScheduledSingleEvent(
                directive=schedule_form.directive,
                id=schedule_form.id,
                next_occurrence_datetime=datetime.datetime.combine(date=schedule_form.date, time=schedule_form.time),
            )
        else:
            return ScheduledRepeatingEvent(
                directive=schedule_form.directive,
                id=schedule_form.id,
                next_occurrence_datetime=datetime.datetime.combine(date=schedule_form.date, time=schedule_form.time),
                play_duration=schedule_form.play_duration,
                weekdays_to_repeat_on=schedule_form.repeat_on_weekdays,
                until=schedule_form.until_date,
            )

    ###############
    ### SCREENS ###
    ###############

    @staticmethod
    def _login_screen(message: Message) -> State:
        message.reply_text(TelegramBot.start_message_text)
        return LOGIN_STATE

    @staticmethod
    def _main_menu_screen(message: Message) -> State:
        message.reply_text(text='Main menu.', reply_markup=MAIN_MENU_BUTTONS_MARKUP)
        return MAIN_MENU_STATE

    @staticmethod
    def _current_default_screen(chat_state: ChatState, message: Message) -> State:
        if TelegramBot._get_current_default_state(chat_state) == MAIN_MENU_STATE:
            return TelegramBot._main_menu_screen(message=message)
        else:
            return TelegramBot._login_screen(message=message)

    @staticmethod
    def _help_screen(chat_state: ChatState, message: Message) -> State:
        if (authentication_info := chat_state.client_authentication_info) is None:
            return TelegramBot._login_screen(message=message)
        else:
            message_text = (
                f'You are currently controlling client_id={authentication_info.client_id}.\n'
                'To reset client_id use /start command'
            )
            message.reply_text(text=message_text)
            return TelegramBot._main_menu_screen(message=message)

    @staticmethod
    def _passphrase_screen(message: Message) -> State:
        message.reply_text('Now enter your passphrase.')
        return PASSWORD_STATE

    @staticmethod
    def _schedule_screen(chat_state: ChatState, message: Message) -> State:
        chat_state.active_form = ScheduleForm()
        message.reply_text(
            text='Enter a name for the scheduled event or get a random one',
            reply_markup=SCHEDULE_ID_BUTTONS_MARKUP
        )
        return SCHEDULE_ID_STATE

    @staticmethod
    def _play_screen(chat_state: ChatState, message: Message) -> State:
        chat_state.active_form = PlayForm()
        message.reply_text(
            text='Choose type of playback',
            reply_markup=PLAY_CHOOSE_PLAYBACK_TYPE_BUTTONS_MARKUP
        )
        return PLAY_DIRECTIVE_STATE

    @staticmethod
    def _status_screen(chat_state: ChatState, message: Message) -> State:
        client_id = chat_state.client_authentication_info.client_id
        with global_lock:
            scheduled_events_previews = \
                '\n\n----------\n'.join(str(se) for se in client_id_to_client_info[client_id].schedule)

        reply_text = (
            'Status of the player is not implemented yet. Sorry :(\n'
            f'Scheduled events are:\n'
            '----------\n'
            f'{scheduled_events_previews}\n'
            '----------'
        )

        message.reply_text(text=reply_text, reply_markup=STATUS_BUTTONS_MARKUP)
        return CURRENT_STATUS_STATE

    @staticmethod
    def _scheduled_event_date_screen(message: Message) -> State:
        message.reply_text(
            text='Enter the date of the first occurrence of the scheduled event (DD.MM.YY)',
            reply_markup=CANCEL_BUTTON_MARKUP
        )
        return SCHEDULE_DATE_STATE

    @staticmethod
    def _scheduled_event_time_screen(message: Message) -> State:
        message.reply_text(
            text='Enter the time of the scheduled event. The format is HH:MM',
            reply_markup=CANCEL_BUTTON_MARKUP
        )
        return SCHEDULE_TIME_STATE

    @staticmethod
    def _scheduled_event_choose_directive_type_screen(message: Message) -> State:
        message.reply_text(
            text='Now choose the directive you want to schedule',
            reply_markup=SCHEDULE_DIRECTIVE_BUTTONS_MARKUP,
        )
        return SCHEDULE_DIRECTIVE_STATE

    @staticmethod
    def _scheduled_event_confirm_screen(chat_state: ChatState, message: Message) -> State:
        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        scheduled_event_preview = str(TelegramBot._make_scheduled_event(schedule_form=schedule_form))

        message.reply_text('Almost done! Check the details and confirm that everything is correct.')
        message.reply_text(text=scheduled_event_preview, reply_markup=SCHEDULE_CONFIRM_BUTTONS_MARKUP)
        return SCHEDULE_CONFIRM_STATE

    @staticmethod
    def _show_choose_directive_screen(message: Message) -> None:
        message_text = (
            'Now give me information about the track/playlist/artist/album.\n'
            'The most simple way is to copy-paste the url from the browser '
            'or the url given by the app when clicking the share button.\n'
            '(Alternatively you may give me bare ids or skip that music.yandex.ru part.)'
        )
        message.reply_text(text=message_text, reply_markup=CANCEL_BUTTON_MARKUP)

    @staticmethod
    def _scheduled_event_choose_weekdays_to_repeat_on_screen(message: Message) -> State:
        message.reply_text(
            text='Now specify the weekdays to repeat on. For now the format is numbers from 0 to 6 separated '
                 'by a space. 0 is Monday, 6 is Sunday.\nThere will be buttons here some day, sorry :)',
            reply_markup=SCHEDULE_REPEAT_ON_WEEKDAYS_BUTTONS_MARKUP,
        )
        return SCHEDULE_REPEAT_ON_WEEKDAYS_STATE

    @staticmethod
    def _scheduled_event_choose_play_duration_screen(message: Message) -> State:
        message.reply_text(
            text='Now enter the play duration. The format is "XXh YYm", like "3h 45m". '
                 '"2h" or "15m" are also both valid.',
            reply_markup=SCHEDULE_PLAY_DURATION_BUTTONS_MARKUP,
        )
        return SCHEDULE_PLAY_DURATION_STATE

    @staticmethod
    def _scheduled_event_choose_until_date_screen(message: Message) -> State:
        message.reply_text(
            text='Now specify the until-date if you want the scheduled event to be auto-canceled at some point.\n'
            'The format is DD.MM.YY.',
            reply_markup=SCHEDULE_UNTIL_DATE_BUTTONS_MARKUP,
        )
        return SCHEDULE_UNTIL_DATE_STATE

    @staticmethod
    def _delete_schedule_entry_screen(message: Message) -> State:
        message.reply_text(
            text='Enter the id of the scheduled event you want to delete.',
            reply_markup=CANCEL_BUTTON_MARKUP,
        )
        return CURRENT_STATUS_DELETE_ENTRY_STATE

    ###############
    ### PARSERS ###
    ###############

    @staticmethod
    def _parse_login(chat_state: ChatState, message: Message) -> bool:
        if not message.text.isnumeric():
            message.reply_text(
                f'Could not parse the login (got "{message.text}").\n'
                f'Notice, that client_id must be a number. Try again.'
            )
            return False

        message.reply_text('OK')
        login_form = tp.cast(LoginForm, chat_state.active_form)
        login_form.login = int(message.text)
        return True

    @staticmethod
    def _parse_passphrase(chat_state: ChatState, message: Message) -> bool:
        login_form = tp.cast(LoginForm, chat_state.active_form)
        login_form.passphrase_hash = hashlib.sha1(message.text.encode()).hexdigest()

        auth_info = ClientAuthenticationInfo(client_id=login_form.login, passphrase_hash=login_form.passphrase_hash)
        chat_state.client_authentication_info = auth_info

        TelegramBot._validate_authentication_info(chat_state)

        if chat_state.client_authentication_info is not None:
            message.reply_text(f'Success! You are now controlling client with client_id={auth_info.client_id}')
            return True
        else:
            message.reply_text('Authentication failed.')
            return False

    @staticmethod
    def _parse_scheduled_event_id(chat_state: ChatState, message: Message, scheduled_event_id: str) -> bool:
        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        schedule_form.id = scheduled_event_id
        message.reply_text(f'Ok! (got {scheduled_event_id=})')
        return True

    @staticmethod
    def _parse_scheduled_event_date(chat_state: ChatState, message: Message) -> bool:
        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        try:
            schedule_form.date = datetime.datetime.strptime(message.text, '%d.%m.%y').date()
            message.reply_text('OK')
            return True
        except ValueError:
            message.reply_text('Failed, try again.')
            return False

    @staticmethod
    def _parse_scheduled_event_time(chat_state: ChatState, message: Message) -> bool:
        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        try:
            schedule_form.time = datetime.datetime.strptime(message.text, '%H:%M').time()
            message.reply_text(text='OK')
            return True
        except ValueError:
            message.reply_text(
                text='Could not parse the time. Try again.',
                reply_markup=CANCEL_BUTTON_MARKUP
            )
            return False

    @staticmethod
    def _parse_scheduled_event_pause_directive(chat_state: ChatState) -> bool:
        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        schedule_form.directive = AudioPlayerPauseDirective()
        return True

    @staticmethod
    def _parse_directive_type(chat_state: ChatState, callback_data: str) -> bool:
        form = tp.cast(PlayForm, chat_state.active_form)
        form.directive_type = DirectiveType.from_callback_data_value(callback_data_value=callback_data)
        return True

    @staticmethod
    def _parse_directive(chat_state: ChatState, message: Message) -> bool:
        form = tp.cast(PlayForm, chat_state.active_form)
        url = TelegramBot._cut_yandex_music_url(message.text)
        if (directive := TelegramBot._parse_any(message_text=url, directive_type=form.directive_type)) is not None:
            form.directive = directive
            message.reply_text('Cool.')
            return True
        else:
            message.reply_text(text='Could not parse the directive. Try again.')
            return False

    @staticmethod
    def _parse_scheduled_event_weekdays(chat_state: ChatState, message: Message) -> bool:
        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        try:
            weekdays = sorted({int(el) for el in message.text.split()})
            assert all(weekday in range(7) for weekday in weekdays)
            schedule_form.repeat_on_weekdays = weekdays
            message.reply_text('Ok')
            return True
        except (ValueError, AssertionError):
            message.reply_text('Failed to parse weekdays. Try again.')
            return False

    @staticmethod
    def _parse_scheduled_event_play_duration(chat_state: ChatState, message: Message) -> bool:
        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        try:
            parts = {part[-1]: int(part[:-1]) for part in message.text.split(' ')}
            assert parts.get('h') or parts.get('m')
            schedule_form.play_duration = datetime.timedelta(hours=parts.get('h', 0), minutes=parts.get('m', 0))

            message.reply_text('OK')
            return True
        except Exception:  # noqa
            message.reply_text(text='Could not parse the play-duration')
            return False

    @staticmethod
    def _parse_until_date(chat_state: ChatState, message: Message) -> bool:
        try:
            schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
            schedule_form.until_date = datetime.datetime.strptime(message.text, '%d.%m.%y').date()
            message.reply_text('OK')
            return True
        except Exception:  # noqa
            message.reply_text('Could not parse the until-date. Try again.')
            return False

    @staticmethod
    def _parse_scheduled_event_confirm(chat_state: ChatState, message: Message) -> bool:
        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        scheduled_event = TelegramBot._make_scheduled_event(schedule_form)
        TelegramBot._schedule_event(chat_state=chat_state, scheduled_event=scheduled_event)
        message.reply_text(
            text='Success! The event has been added to the schedule. You can see all your scheduled '
                 'events by clicking on the status button in the main menu.'
        )
        return True

    @staticmethod
    def _parse_clear_scheduled_events(chat_state: ChatState, message: Message) -> bool:
        client_id = chat_state.client_authentication_info.client_id
        with global_lock:
            client_id_to_client_info[client_id].schedule.clear()

        message.reply_text(
            text='Successfully deleted all the scheduled events!',
            reply_markup=MAIN_MENU_BUTTONS_MARKUP,
        )
        return True

    @staticmethod
    def _parse_scheduled_event_id_for_deleting(chat_state: ChatState, message: Message) -> bool:
        delete_scheduled_event_form = tp.cast(DeleteScheduledEventForm, chat_state.active_form)
        delete_scheduled_event_form.schedule_id = message.text

        client_id = chat_state.client_authentication_info.client_id
        with global_lock:
            client_id_to_client_info[client_id].schedule = [
                scheduled_event
                for scheduled_event in client_id_to_client_info[client_id].schedule
                if scheduled_event.id != delete_scheduled_event_form.schedule_id
            ]
        message.reply_text(text='Done.')
        return True

    ################
    ### HANDLERS ###
    ################

    @staticmethod
    def cancel_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        chat_state = TelegramBot._chat_state(context)
        TelegramBot._validate_authentication_info(chat_state)
        return TelegramBot._current_default_screen(chat_state=chat_state, message=update.callback_query.message)

    @staticmethod
    def start_handler(update: Update, context: CallbackContext) -> State:
        context.chat_data.update({'state': ChatState(active_form=LoginForm())})
        return TelegramBot._login_screen(message=update.message)

    @staticmethod
    def help_handler(update: Update, context: CallbackContext) -> State:
        chat_state = TelegramBot._chat_state(context)
        TelegramBot._validate_authentication_info(chat_state=chat_state)
        return TelegramBot._help_screen(chat_state=chat_state, message=update.message)

    @staticmethod
    def login_handler(update: Update, context: CallbackContext) -> State:
        if TelegramBot._parse_login(chat_state=TelegramBot._chat_state(context), message=update.message):
            return TelegramBot._passphrase_screen(message=update.message)
        else:
            return TelegramBot._login_screen(message=update.message)

    @staticmethod
    def passphrase_handler(update: Update, context: CallbackContext) -> State:
        if TelegramBot._parse_passphrase(chat_state=TelegramBot._chat_state(context), message=update.message):
            return TelegramBot._main_menu_screen(message=update.message)
        else:
            return TelegramBot._login_screen(message=update.message)

    @staticmethod
    def button_resume_handler(update: Update, context: CallbackContext) -> State:
        TelegramBot._push_directive(context=context, directive=AudioPlayerResumeDirective())
        update.callback_query.answer()
        return MAIN_MENU_STATE

    @staticmethod
    def button_pause_handler(update: Update, context: CallbackContext) -> State:
        TelegramBot._push_directive(context=context, directive=AudioPlayerPauseDirective())
        update.callback_query.answer()
        return MAIN_MENU_STATE

    @staticmethod
    def button_next_track_handler(update: Update, context: CallbackContext) -> State:
        TelegramBot._push_directive(context=context, directive=AudioPlayerNextTrackDirective())
        update.callback_query.answer()
        return MAIN_MENU_STATE

    @staticmethod
    def button_previous_track_handler(update: Update, context: CallbackContext) -> State:
        TelegramBot._push_directive(context=context, directive=AudioPlayerPrevTrackDirective())
        update.callback_query.answer()
        return MAIN_MENU_STATE

    @staticmethod
    def main_menu_button_schedule_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        return TelegramBot._schedule_screen(
            chat_state=TelegramBot._chat_state(context),
            message=update.callback_query.message
        )

    @staticmethod
    def main_menu_button_play_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        return TelegramBot._play_screen(
            chat_state=TelegramBot._chat_state(context),
            message=update.callback_query.message
        )

    @staticmethod
    def main_menu_button_status_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        return TelegramBot._status_screen(
            chat_state=TelegramBot._chat_state(context),
            message=update.callback_query.message
        )

    @staticmethod
    def generate_random_id_for_scheduled_event_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        TelegramBot._parse_scheduled_event_id(
            chat_state=TelegramBot._chat_state(context),
            message=update.callback_query.message,
            scheduled_event_id=str(random.randint(0, 1_000_000_000))
        )
        return TelegramBot._scheduled_event_date_screen(message=update.callback_query.message)

    @staticmethod
    def parse_scheduled_event_id_handler(update: Update, context: CallbackContext) -> State:
        TelegramBot._parse_scheduled_event_id(
            chat_state=TelegramBot._chat_state(context),
            message=update.message,
            scheduled_event_id=update.message.text
        )
        return TelegramBot._scheduled_event_date_screen(message=update.message)

    @staticmethod
    def parse_scheduled_event_date_handler(update: Update, context: CallbackContext) -> State:
        if TelegramBot._parse_scheduled_event_date(chat_state=TelegramBot._chat_state(context), message=update.message):
            return TelegramBot._scheduled_event_time_screen(message=update.message)
        else:
            return TelegramBot._scheduled_event_date_screen(message=update.message)

    @staticmethod
    def parse_scheduled_event_time_handler(update: Update, context: CallbackContext) -> State:
        if TelegramBot._parse_scheduled_event_time(chat_state=TelegramBot._chat_state(context), message=update.message):
            return TelegramBot._scheduled_event_choose_directive_type_screen(message=update.message)
        else:
            return TelegramBot._scheduled_event_time_screen(message=update.message)

    @staticmethod
    def parse_scheduled_event_pause_directive_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()

        chat_state = TelegramBot._chat_state(context)
        assert TelegramBot._parse_scheduled_event_pause_directive(chat_state=chat_state)
        return TelegramBot._scheduled_event_confirm_screen(chat_state=chat_state, message=update.callback_query.message)

    @staticmethod
    def parse_scheduled_event_directive_type_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()

        assert TelegramBot._parse_directive_type(
            chat_state=TelegramBot._chat_state(context),
            callback_data=update.callback_query.data
        )
        TelegramBot._show_choose_directive_screen(message=update.callback_query.message)
        return SCHEDULE_DIRECTIVE_PLAY_STATE

    @staticmethod
    def parse_scheduled_event_play_directive_handler(update: Update, context: CallbackContext) -> State:
        if TelegramBot._parse_directive(chat_state=TelegramBot._chat_state(context), message=update.message):
            return TelegramBot._scheduled_event_choose_weekdays_to_repeat_on_screen(message=update.message)
        else:
            TelegramBot._show_choose_directive_screen(message=update.message)
            return SCHEDULE_DIRECTIVE_PLAY_STATE

    @staticmethod
    def parse_scheduled_event_weekdays_handler(update: Update, context: CallbackContext) -> State:
        message = update.message
        if TelegramBot._parse_scheduled_event_weekdays(chat_state=TelegramBot._chat_state(context), message=message):
            return TelegramBot._scheduled_event_choose_play_duration_screen(message=message)
        else:
            return TelegramBot._scheduled_event_choose_weekdays_to_repeat_on_screen(message=message)

    @staticmethod
    def scheduled_event_no_repeat_handler(update: Update, _: CallbackContext) -> State:
        update.callback_query.answer()
        update.callback_query.message.reply_text(text='Ok, won\'t repeat this event weekly, will just run it once.')
        return TelegramBot._scheduled_event_choose_play_duration_screen(message=update.callback_query.message)

    @staticmethod
    def parse_scheduled_event_play_duration_handler(update: Update, context: CallbackContext) -> State:
        chat_state = TelegramBot._chat_state(context)
        if not TelegramBot._parse_scheduled_event_play_duration(chat_state=chat_state, message=update.message):
            return TelegramBot._scheduled_event_choose_play_duration_screen(message=update.message)

        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        if schedule_form.repeat_on_weekdays:
            return TelegramBot._scheduled_event_choose_until_date_screen(message=update.message)
        else:
            return TelegramBot._scheduled_event_confirm_screen(chat_state=chat_state, message=update.message)

    @staticmethod
    def schedule_unlimited_play_duration_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        update.callback_query.message.reply_text('Ok, the music won\'t be stopped automatically.')

        chat_state = TelegramBot._chat_state(context)
        schedule_form = tp.cast(ScheduleForm, chat_state.active_form)
        if schedule_form.repeat_on_weekdays:
            return TelegramBot._scheduled_event_choose_until_date_screen(message=update.callback_query.message)
        else:
            return TelegramBot._scheduled_event_confirm_screen(
                chat_state=chat_state,
                message=update.callback_query.message
            )

    @staticmethod
    def schedule_no_until_date_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()

        message = update.callback_query.message
        message.reply_text(text='Ok, no until-date. You will still be able to cancel this event manually though.')

        return TelegramBot._scheduled_event_confirm_screen(chat_state=TelegramBot._chat_state(context), message=message)

    @staticmethod
    def parse_until_date_handler(update: Update, context: CallbackContext) -> State:
        chat_state = TelegramBot._chat_state(context)
        if TelegramBot._parse_until_date(chat_state=chat_state, message=update.message):
            return TelegramBot._scheduled_event_confirm_screen(chat_state=chat_state, message=update.message)
        else:
            return TelegramBot._scheduled_event_choose_until_date_screen(message=update.message)

    @staticmethod
    def confirm_scheduled_event_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        message = update.callback_query.message
        assert TelegramBot._parse_scheduled_event_confirm(chat_state=TelegramBot._chat_state(context), message=message)
        return TelegramBot._main_menu_screen(message=message)

    @staticmethod
    def parse_play_event_directive_type_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        play_form = tp.cast(PlayForm, TelegramBot._chat_state(context).active_form)
        play_form.directive_type = DirectiveType.from_callback_data_value(update.callback_query.data)

        TelegramBot._show_choose_directive_screen(message=update.callback_query.message)
        return PLAY_DIRECTIVE_PLAY_STATE

    @staticmethod
    def parse_play_event_play_directive_handler(update: Update, context: CallbackContext) -> State:
        chat_state = TelegramBot._chat_state(context)
        if TelegramBot._parse_directive(chat_state=chat_state, message=update.message):
            play_form = tp.cast(PlayForm, chat_state.active_form)
            TelegramBot._push_directive(context=context, directive=play_form.directive)
            update.message.reply_text(text='Done!')
            return TelegramBot._main_menu_screen(message=update.message)
        else:
            TelegramBot._show_choose_directive_screen(message=update.message)
            return PLAY_DIRECTIVE_PLAY_STATE

    @staticmethod
    def delete_schedule_entry_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        chat_state = TelegramBot._chat_state(context)
        chat_state.active_form = DeleteScheduledEventForm()

        return TelegramBot._delete_schedule_entry_screen(message=update.callback_query.message)

    @staticmethod
    def clear_scheduled_events_handler(update: Update, context: CallbackContext) -> State:
        update.callback_query.answer()
        TelegramBot._parse_clear_scheduled_events(
            chat_state=TelegramBot._chat_state(context),
            message=update.callback_query.message
        )
        return TelegramBot._main_menu_screen(message=update.callback_query.message)

    @staticmethod
    def parse_scheduled_event_id_to_delete_handler(update: Update, context: CallbackContext) -> State:
        assert TelegramBot._parse_scheduled_event_id_for_deleting(
            chat_state=TelegramBot._chat_state(context),
            message=update.message
        )
        return TelegramBot._main_menu_screen(message=update.message)

    #################
    ### INTERFACE ###
    #################

    def start_polling(self) -> None:
        self._updater.start_polling()

    def stop(self) -> None:
        self._updater.stop()
