# TODO(igor-darov): either remove this or reuse in the new telegram bot
def reply_to_callback_query(update, message, reply_markup):
    update.callback_query.answer()
    text = f'{update.callback_query.message.text.splitlines()[0]}\nLast command: {message}'
    if text != update.callback_query.message.text:
        update.callback_query.edit_message_text(
            text=text,
            reply_markup=reply_markup
        )
