from enum import Enum


class CallbackData(Enum):
    # common player callbacks data
    PREV = 'prev'
    NEXT = 'next'
    PAUSE = 'pause'
    RESUME = 'resume'

    # pick type of play-directive callbacks data
    PLAYLIST = 'directive_type_playlist'
    ARTIST = 'directive_type_artist'
    ALBUM = 'directive_type_album'
    TRACK = 'directive_type_track'

    # cancel / go to the main menu callback data
    CANCEL = 'cancel'

    # main menu callbacks data
    PLAY = 'play'
    SCHEDULE = 'schedule'
    STATUS = 'status'

    # schedule callbacks data
    SCHEDULE_GENERATE_RANDOM_ID = 'schedule_generate_random_id'
    SCHEDULE_PAUSE = 'schedule_pause'
    SCHEDULE_UNLIMITED_PLAY_DURATION = 'schedule_unlimited_play_duration'
    SCHEDULE_DO_NOT_REPEAT = 'schedule_do_not_repeat'
    SCHEDULE_NO_UNTIL_DATE = 'schedule_no_until_date'
    SCHEDULE_CONFIRM = 'schedule_confirm'

    # status callbacks data
    STATUS_DELETE_SCHEDULED_EVENT = 'status_delete_scheduled_event'
    STATUS_CLEAR_SCHEDULED_EVENTS = 'status_clear_scheduled_events'

    @property
    def pattern(self) -> str:
        return f'^{self.value}$'
