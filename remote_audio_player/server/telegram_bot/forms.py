from __future__ import annotations

import datetime
import typing as tp
from dataclasses import dataclass
from enum import Enum

from .callbacks_data import CallbackData
from ...directives import AudioPlayerDirective


class Form:
    def clear(self) -> None:
        for key in self.__dict__:
            setattr(self, key, None)


@dataclass
class LoginForm(Form):
    login: tp.Optional[int] = None
    passphrase_hash: tp.Optional[str] = None


class DirectiveType(Enum):
    ARTIST = 0
    PLAYLIST = 1
    TRACK = 2
    ALBUM = 3

    @classmethod
    def from_callback_data_value(cls, callback_data_value: str) -> DirectiveType:
        return {
            CallbackData.ARTIST.value: cls.ARTIST,
            CallbackData.PLAYLIST.value: cls.PLAYLIST,
            CallbackData.TRACK.value: cls.TRACK,
            CallbackData.ALBUM.value: cls.ALBUM,
        }[callback_data_value]


@dataclass
class PlayForm(Form):
    directive_type: tp.Optional[DirectiveType] = None
    directive: tp.Optional[AudioPlayerDirective] = None


@dataclass
class ScheduleForm(PlayForm):
    id: tp.Optional[str] = None
    date: tp.Optional[datetime.date] = None
    time: tp.Optional[datetime.time] = None
    repeat_on_weekdays: tp.Optional[list[int]] = None
    play_duration: tp.Optional[datetime.timedelta] = None
    until_date: tp.Optional[datetime.date] = None


@dataclass
class DeleteScheduledEventForm(Form):
    schedule_id: tp.Optional[str] = None
