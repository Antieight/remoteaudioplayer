import datetime
import hashlib
import random
import requests

from threading import Thread

from flask import Flask, request
from werkzeug.serving import run_simple

from ..config import (
    COLLECT_REQUEST_HANDLE, STORE_REQUEST_HANDLE, REGISTER_NEW_CLIENT_ID_HANDLE, TRY_AUTHENTICATE,
    CLIENT_ID_KEY, PASSPHRASE_KEY, DIRECTIVE_KEY,
)
from ..directives import serialize_audio_player_directive, deserialize_audio_player_directive
from .state import global_lock, client_id_to_client_info, ClientInfo


app = Flask(__name__)

EMPTY_OK_RESPONSE = ('', requests.codes.NO_CONTENT)
EMPTY_BAD_REQUEST_RESPONSE = ('', requests.codes.BAD_REQUEST)


def _do_authenticate(client_id, password) -> bool:
    return (
        isinstance(password, str) and
        client_id in client_id_to_client_info and
        client_id_to_client_info[client_id].passphrase_hash == hashlib.sha1(password.encode()).hexdigest()
    )


@app.route(f'/{REGISTER_NEW_CLIENT_ID_HANDLE}', methods=['POST'])
def register_new_client():
    with global_lock:
        client_id = request.json.get(CLIENT_ID_KEY)
        passphrase = request.json.get(PASSPHRASE_KEY)
        tz_timedelta_hours = request.json.get('tz_timedelta_hours')

        if client_id is None:
            # TODO(igor-darov): think of a smarter way to give out client ids maybe
            client_id = max(client_id_to_client_info) + 1 if client_id_to_client_info else 0

        if passphrase is None:
            passphrase = str(random.randint(1_000_000_000, 10_000_000_000))

        if (
            not isinstance(client_id, int) or
            not isinstance(tz_timedelta_hours, int) or
            not -12 <= tz_timedelta_hours <= 12 or
            client_id in client_id_to_client_info or
            passphrase is None
        ):
            return EMPTY_BAD_REQUEST_RESPONSE

        client_id_to_client_info[client_id] = ClientInfo(
            client_id=client_id,
            passphrase_hash=hashlib.sha1(passphrase.encode()).hexdigest(),
            tz_timedelta_hours=tz_timedelta_hours,
        )

        return {'client_id': client_id, 'passphrase': passphrase}


@app.route(f'/{TRY_AUTHENTICATE}', methods=['POST'])
def try_authenticate():
    with global_lock:
        client_id = request.json.get(CLIENT_ID_KEY)
        password = request.json.get(PASSPHRASE_KEY, '')
        return {'result': _do_authenticate(client_id=client_id, password=password)}


@app.route(f"/{COLLECT_REQUEST_HANDLE}/<client_id_str>")
def collect_request(client_id_str: str):
    with global_lock:
        # TODO(igor-darov): enable this back somehow
        # if not _do_authenticate(client_id=client_id, password=request.json.get(PASSPHRASE_KEY, '')):
        #     return '', requests.codes.UNAUTHORIZED

        if not client_id_str.isnumeric():
            return EMPTY_BAD_REQUEST_RESPONSE

        client_id = int(client_id_str)

        if this_client_queue := client_id_to_client_info[client_id].directives_queue:
            return serialize_audio_player_directive(this_client_queue.popleft())
        else:
            return EMPTY_OK_RESPONSE


@app.route(f'/{STORE_REQUEST_HANDLE}/<client_id_str>', methods=['POST'])
def store(client_id_str: str):

    if not client_id_str.isnumeric():
        return '', requests.codes.UNAUTHORIZED
    client_id = int(client_id_str)

    with global_lock:
        password = request.json.get(PASSPHRASE_KEY, '')
        if not _do_authenticate(client_id=client_id, password=password):
            return '', requests.codes.UNAUTHORIZED

        try:
            deserialized_directive = deserialize_audio_player_directive(request.json.get(DIRECTIVE_KEY))
        except ValueError:
            return EMPTY_BAD_REQUEST_RESPONSE

        client_id_to_client_info[int(client_id)].directives_queue.append(deserialized_directive)
        return EMPTY_OK_RESPONSE


def start_flask_server(port: int = 9000):
    Thread(target=lambda: run_simple(hostname='0.0.0.0', port=port, application=app)).start()
