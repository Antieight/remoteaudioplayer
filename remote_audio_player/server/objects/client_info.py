import datetime

from collections import deque
from dataclasses import dataclass, field

from ...directives import AudioPlayerDirective
from .scheduled_event import ScheduledEvent


@dataclass
class ClientInfo:
    client_id: int
    passphrase_hash: str
    tz_timedelta_hours: int
    directives_queue: deque[AudioPlayerDirective] = field(default_factory=deque)
    schedule: list[ScheduledEvent] = field(default_factory=list)
