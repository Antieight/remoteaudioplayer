import os

import typing as tp

import click

from ..audio_player import YandexMusicAudioPlayer
from .client import Client


@click.command()
@click.option('-s', '--server-url', required=True)
@click.option('-p', '--port', required=True, type=int)
@click.option('-y', '--yandex-music-session-id', default=lambda: os.environ.get('YANDEX_MUSIC_SESSION_ID', ''),
              help='Session id from music.yandex.ru; Can be obtained from Session_id or sessionid2 cookie; '
                   'By default is read from env variable YANDEX_MUSIC_SESSION_ID')
@click.option('--ask-every-n-seconds', default=5, show_default=True,
              help='How ofter the client will hit the server asking for commands')
@click.option('-u', '--user-name', default='igor-darov', show_default=True,
              help='User name whose playlists will be played by default')
@click.option('-t', '--timeout', default=1, show_default=True, help='Timeout for server requests')
@click.option('-c', '--client-id', default=None, type=int, help='Client id that was handed out earlier.'
                                                                'Omit on first run.')
@click.option('--passphrase', default=None, type=str, help='Passphrase for the specified client id')
def start_client(server_url: str, port: int, yandex_music_session_id: str,
                 ask_every_n_seconds: int, user_name: str, timeout: int,
                 client_id: tp.Optional[int], passphrase: tp.Optional[str]) -> None:
    try:
        player = YandexMusicAudioPlayer(user_name=user_name, session_id=yandex_music_session_id)
    except AssertionError as e:
        print(f'Could not start client: {str(e)}')
        return

    client = Client(player=player, server_address=server_url, port=port,
                    ask_every_seconds=ask_every_n_seconds, timeout=timeout,
                    client_id=client_id, passphrase=passphrase)
    client.poll()
