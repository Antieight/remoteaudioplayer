import typing as tp

from dataclasses import dataclass

import requests
import time

from ..audio_player import IPlayer
from ..config import (
    COLLECT_REQUEST_HANDLE, REGISTER_NEW_CLIENT_ID_HANDLE, TRY_AUTHENTICATE,
    CLIENT_ID_KEY, PASSPHRASE_KEY, TZ_TIMEDELTA_HOURS_KEY,
)
from ..directives import AudioPlayerDirective, deserialize_audio_player_directive


@dataclass
class Client:
    player: IPlayer

    server_address: str
    port: int
    ask_every_seconds: int = 5

    client_id: tp.Optional[int] = None
    passphrase: tp.Optional[str] = None

    timeout: int = 1

    def __post_init__(self) -> None:
        if self.client_id is None:
            self._register_and_receive_new_client_id()

        if not self._try_authenticate():
            raise RuntimeError('Could not authenticate with the given client_id and passphrase')

        print(f'Started Client with client_id="{self.client_id}" and passphrase="{self.passphrase}"')

    def _register_and_receive_new_client_id(self, n_retries: int = 3) -> None:
        url = f'{self.server_address}:{self.port}/{REGISTER_NEW_CLIENT_ID_HANDLE}'
        current_client_tz_timedelta_hours = (
            - (time.timezone if (time.localtime().tm_isdst == 0) else time.altzone)
            //
            3600
        )
        for _ in range(n_retries):
            if (response := requests.post(url, json={TZ_TIMEDELTA_HOURS_KEY: current_client_tz_timedelta_hours})).ok:
                self.client_id = response.json()[CLIENT_ID_KEY]
                self.passphrase = response.json()[PASSPHRASE_KEY]
                return
        raise RuntimeError('Failed to register and get a client_id/passphrase pair')

    def _try_authenticate(self, n_retries: int = 3) -> bool:
        try:
            url = f'{self.server_address}:{self.port}/{TRY_AUTHENTICATE}'
            for _ in range(n_retries):
                response = requests.post(url, json={CLIENT_ID_KEY: self.client_id, PASSPHRASE_KEY: self.passphrase})
                if response.ok and response.json().get('result'):
                    return True
            return False
        except Exception as e:
            raise RuntimeError('Authentication error :(') from e

    def _receive_directive(self) -> tp.Optional[AudioPlayerDirective]:
        try:
            url = f'{self.server_address}:{self.port}/{COLLECT_REQUEST_HANDLE}/{self.client_id}'
            response = requests.get(url, timeout=self.timeout)

            if response.status_code != requests.codes.OK:
                return None

            data = response.json()
            return deserialize_audio_player_directive(data=data)
        except Exception as e:
            print(f'failed to receive directive: {e}')
            return None

    def poll(self) -> None:
        while True:
            time.sleep(self.ask_every_seconds)
            if directive := self._receive_directive():
                print(f'executing directive {directive}')
                directive.apply(self.player)
