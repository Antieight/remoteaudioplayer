COLLECT_REQUEST_HANDLE = 'collect_request'
REGISTER_NEW_CLIENT_ID_HANDLE = 'register_new_client_id'
STORE_REQUEST_HANDLE = 'store_request'
TRY_AUTHENTICATE = 'try_authenticate'

CLIENT_ID_KEY = 'client_id'
PASSPHRASE_KEY = 'passphrase'
TZ_TIMEDELTA_HOURS_KEY = 'tz_timedelta_hours'
DIRECTIVE_KEY = 'directive'
