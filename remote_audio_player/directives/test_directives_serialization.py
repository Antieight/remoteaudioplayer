import pytest

from .directives import (
    AudioPlayerPlayPlaylistDirective, AudioPlayerPlayArtistDirective, AudioPlayerPrevTrackDirective,
    AudioPlayerNextTrackDirective, AudioPlayerResumeDirective, AudioPlayerPauseDirective,
    AudioPlayerPlayAlbumDirective, AudioPlayerPlayTrackDirective,
    serialize_audio_player_directive, deserialize_audio_player_directive
)


@pytest.mark.parametrize('directive', [
    AudioPlayerPlayPlaylistDirective(playlist_id=123, user_name='igor-darov'),
    AudioPlayerPlayPlaylistDirective(playlist_id=123, user_name=None),
    AudioPlayerPlayArtistDirective(artist_id=12345),
    AudioPlayerPlayAlbumDirective(album_id=123),
    AudioPlayerPlayTrackDirective(album_id=123, track_id=456),
    AudioPlayerNextTrackDirective(),
    AudioPlayerPrevTrackDirective(),
    AudioPlayerPauseDirective(),
    AudioPlayerResumeDirective(),
])
def test_serialization(directive):
    serialized = serialize_audio_player_directive(directive)
    deserialized = deserialize_audio_player_directive(serialized)
    assert type(directive) == type(deserialized) and directive.__dict__ == deserialized.__dict__, directive
