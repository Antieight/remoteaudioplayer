import typing as tp

from abc import ABC, abstractmethod
from dataclasses import dataclass

from ..audio_player import IPlayer


class AudioPlayerDirective(ABC):
    name: tp.ClassVar[str] = ''

    @abstractmethod
    def apply(self, player: IPlayer) -> None:
        raise NotImplementedError


@dataclass
class AudioPlayerPlayPlaylistDirective(AudioPlayerDirective):
    playlist_id: int
    user_name: tp.Optional[str] = None

    name: tp.ClassVar[str] = 'play_playlist'

    def apply(self, player: IPlayer) -> None:
        player.play_playlist(playlist_id=self.playlist_id, user_name=self.user_name)


@dataclass
class AudioPlayerPlayArtistDirective(AudioPlayerDirective):
    artist_id: int

    name: tp.ClassVar[str] = 'play_artist'

    def apply(self, player: IPlayer) -> None:
        player.play_artist(artist_id=self.artist_id)


@dataclass
class AudioPlayerPlayTrackDirective(AudioPlayerDirective):
    album_id: int
    track_id: int

    name: tp.ClassVar[str] = 'play_track'

    def apply(self, player: IPlayer) -> None:
        player.play_track(album_id=self.album_id, track_id=self.track_id)


@dataclass
class AudioPlayerPlayAlbumDirective(AudioPlayerDirective):
    album_id: int

    name: tp.ClassVar[str] = 'play_album'

    def apply(self, player: IPlayer) -> None:
        player.play_album(album_id=self.album_id)


@dataclass
class AudioPlayerNextTrackDirective(AudioPlayerDirective):
    name: tp.ClassVar[str] = 'next_track'

    def apply(self, player: IPlayer) -> None:
        player.next_track()


@dataclass
class AudioPlayerPrevTrackDirective(AudioPlayerDirective):
    name: tp.ClassVar[str] = 'prev_track'

    def apply(self, player: IPlayer) -> None:
        player.prev_track()


@dataclass
class AudioPlayerPauseDirective(AudioPlayerDirective):
    name: tp.ClassVar[str] = 'pause'

    def apply(self, player: IPlayer) -> None:
        player.pause()


@dataclass
class AudioPlayerResumeDirective(AudioPlayerDirective):
    name: tp.ClassVar[str] = 'resume'

    def apply(self, player: IPlayer) -> None:
        player.resume()


supported_audio_player_directives = [
    AudioPlayerPlayPlaylistDirective,
    AudioPlayerPlayArtistDirective,
    AudioPlayerPlayAlbumDirective,
    AudioPlayerPlayTrackDirective,
    AudioPlayerNextTrackDirective,
    AudioPlayerPrevTrackDirective,
    AudioPlayerPauseDirective,
    AudioPlayerResumeDirective,
]

player_directive_name_to_cls = {cls.name: cls for cls in supported_audio_player_directives}


def serialize_audio_player_directive(request: AudioPlayerDirective) -> dict:
    return request.__dict__ | {'_directive_name': request.name}


def deserialize_audio_player_directive(data: dict) -> AudioPlayerDirective:
    directive_name = data.pop('_directive_name')
    assert directive_name in player_directive_name_to_cls
    return player_directive_name_to_cls[directive_name](**data)
